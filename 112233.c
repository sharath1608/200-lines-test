#include <stdio.h>
#include "112234.h"

int factorial(int n) {
 int c;
 int result = 1;

 for (c=1;c<=n;c=c+1){
  result = result*c;
 }

 return result;
}

long find_ncr(int n, int r) {
 long result;

 printf("Inside find_ncr: n=%d \t r = %d\n",n,r);

 result = factorial(n)/(factorial(r)*factorial(n-r));

 return result;
}

long find_npr(int n, int r) {
 long result;

 result = factorial(n)/factorial(n-r);

 return result;
}

int test_jump() {
 int a, b, c, d;
 a = 12;
 c = 33;

 if(a == 12){
  b = 26;
  if (c == 33){
   a = 13;
   if (a == 15){
    d = 25;
   } else if (a == 13){
    d = 35;
    return 35;
   }
  }
 }		
 return b+d;
}

int add(int a, int b) {
 int c, d;

 c = a + b;
 d = sub(c, a);
 c = d + a;
 return c;
}

int sub(int x, int y) {
 int r, s, t;
 r = x - y;
 s = sqr(x);
 t = sqr(y);
 r = r + s + t;
 return r;
}

int sqr(int a) {
 int c;
 c = a * a;
 return c;
}

void pascal_triangle() {

 int i, n, c; 
 printf("Enter the number of rows you wish to see in pascal triangle\n");
 scanf("%d",&n);

 for (i = 0; i < n; i++) {
  for (c = 0; c <= (n - i - 2); c++) {
   printf(" ");
  }

  for (c = 0 ; c <= i; c++) {
   printf("%ld ",factorial(i)/(factorial(c)*factorial(i-c)));
  }

  printf("\n");
 }
}

int test_if_else() {
 int a, b, c, d, e, f, i;
 a = 12;
 c = 33;

 if(a == 12) {
  b = 26;
  if (c == 33) {
   a = 13;
   e = 24;
   if (a == 15) {
    d = 25;
   } else if (a == 13) {
    if(e >= 24) {
     f = 2;
     for(i = 0; i < 10; i++) {
      f = f + i;
     }
    } else if(e == 24) {
     f = 3;
    } else {
     f = 4;
    }
    d = 35;
   } else {
    d = 45;
   }
  } else {
   d = 30;
  }
 } else {
  b = 30;
  d = 20;
 }

 i = 101;

 if((b+d+f) == 108) {
  b = 20;
  d = 30;
  e = 40;
 } else {
  b = 40;
  d = 50;
  e = 60;
 }

 return b+d+f+i;
}