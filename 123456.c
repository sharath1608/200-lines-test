#include <stdio.h>
#include "112234.h"
#include "223345.h"
#include "334456.h"

int main(int argc, char * argv[]) {
 int if_res, perfect_res,fact_res, res_lpf, res_jump;
 int a,b,c,d,e,f,g,h,i;
 int n, r;
 long ncr, npr;
 int res = 0;

 if_res = test_if_else();
 perfect_res = is_perfect(if_res);
 fact_res = factorial(4);
 res_lpf = get_largest_prime_factor(fact_res);
 res_jump = test_jump();

 a = 10;
 b = 20;
 d = 30;
 e = 40;
 g = 50;
 h = 60;
 c = add(a,b);
 f = add(d,e);
 i = add(g,h);
 n = b-8;
 r = a;

 printf("n=%d \t r = %d\n",n,r);

 ncr = find_ncr(n, r);
 npr = find_npr(n, r);
 pascal_triangle();

 res = c + f + i + if_res + perfect_res + fact_res + res_lpf + res_jump + ncr + npr;

 return res;
}