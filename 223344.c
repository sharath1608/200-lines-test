#include <stdio.h>
#include "223345.h"

int isprime(int in) {

 if(in<0){
  return 0;
 }

 int i;

 for(i=2;i<in;i++) {
  if(in%i==0) {
   return 0;
  }
 }

 return 1;
}

int get_largest_prime_factor(int in) {
 int i,temp;

 for(i=in/2-1;i>1;i=i-2) {
  if(in%i==0) {
   temp = isprime(i);
   if(temp) {
    return i;
   }
  }
 }

 return 0;
}